from django.db import models

# Create your models here.
class Product(models.Model):
  id = models.AutoField(primary_key = True)
  p_name = models.CharField(max_length=120)
  description = models.TextField(max_length=800)
  image = models.ImageField(null=True, blank=True, upload_to="images/")
  price = models.PositiveIntegerField()
  stock = models.PositiveIntegerField()

  class Meta:
    db_table = 'product'

  def __str__(self):
    return self.p_name
