from django.shortcuts import render
from products.models import Product

# Create your views here.
def HomeView(request):
  getProducts = Product.objects.all()
  data = { 'products' : getProducts }
  return render (request, 'index.html', data)

def tableView(request):
  getProducts = Product.objects.all()
  data = { 'products' : getProducts }
  return render(request, 'tableProduct.html', data)