from django.db import models

# Create your models here.
class Client(models.Model):
  id = models.AutoField(primary_key = True)
  first_name = models.CharField(max_length=200)
  last_name = models.CharField(max_length=200)
  email = models.CharField(max_length=100)
  direction = models.CharField(max_length=120)
  age = models.PositiveIntegerField()

  class Meta:
    db_table = 'client'

  def __str__(self):
    return self.first_name + ' ' + self.last_name