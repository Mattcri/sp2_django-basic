from django.shortcuts import render
from clients.models import Client

# Create your views here.
def clientView(request):
  getClients = Client.objects.all()
  data = { 'clients' : getClients }
  return render(request, 'client.html', data)